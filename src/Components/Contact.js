import React, {useState } from 'react';
import { useForm, ValidationError } from '@formspree/react';
import ConfirmButton from './Button';
import './Form.css';
import 'react-datepicker/dist/react-datepicker.css';
import DatePicker from 'react-datepicker';

export default function Form() {
  const [state, handleSubmit] = useForm("xknaylaj");
  const [selectedDate, setSelectedDate] = useState(0);

  if (state.succeeded) {
    return <ConfirmButton />;
  }

  return (
    <div className="form-container">
      <h2 className="form-title">Rejuvenate your<span class="text-indigo-500"> chakras</span> today</h2>
      <p className="form-subtitle">Get your next session free, on us</p>
      <form onSubmit={handleSubmit} className="form">
        <div className="form-group">
          <label htmlFor="first-name" className="form-label">
            Full Name
          </label>
          <div className="form-input-wrapper">
            <input
              type="text"
              name="full-name"
              id="fulll-name"
              autoComplete="full-name"
              className="form-input"
            />
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="email" className="form-label">
            Email address
          </label>
          <div className="form-input-wrapper">
            <input
              type="email"
              name="email"
              id="email"
              autoComplete="email"
              className="form-input"
              required
            />
            <ValidationError prefix="Email" field="email" errors={state.errors} />
          </div>
        </div>
        <div className="sm:col-span-2">
                <label htmlFor="phone-number" className="form-label">
                  Phone number
                </label>
                <div className="mt-2.5">
                  <input
                    type="text"
                    name="phone number"
                    id="phone number"
                    autoComplete="phone number"
                    className="form-input"
                    required
                  />
                </div>
              </div>
              <div className="mt-5">
                  <label htmlFor="appointment" className="form-label">
                    Pick your appointment date
                  </label>
                  <div className="mt-2.5">
                    <DatePicker
                    type="number"
                    name="appointment date"
                      id="appointment date"
                      selected={selectedDate}
                      onChange={(date) => setSelectedDate(date)}
                      minDate={new Date()}
                      required
                      className="form-input"
                    />
                  </div>
                </div>
        <div className="form-group">
          <label htmlFor="message" className="form-label">
            Message
          </label>
          <div className="form-input-wrapper">
            <textarea
              name="message"
              id="message"
              rows={4}
              className="form-input"
            />
          </div>
        </div>
        <button type="submit" className="form-submit-button">
          Submit
        </button>
      </form>
    </div>
  );
}
