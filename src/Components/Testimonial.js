import React from "react";
import ProfilePic from "../Assets/john-doe-image.png";
import { AiFillStar } from "react-icons/ai";

const Testimonial = () => {
  return (
    <div className="work-section-wrapper">
      <div className="work-section-top">
        <h1 className="primary-heading">What They Are Saying</h1>
        <p className="primary-text">
          Whether you need to unwind after a long day at work or want to pamper yourself with a spa day, we have everything you need to feel relaxed and refreshed.
        </p>
      </div>
      <div className="testimonial-section-bottom">
        <div className="testimonial-image">
        <img src={ProfilePic} alt=""/>
        </div>
        <p>
          The therapist really took the time to understand my specific needs and preferences, and tailored the massage accordingly. I left feeling relaxed, refreshed, and rejuvenated. I would highly recommend this spa to anyone looking for a truly therapeutic and enjoyable massage experience.
        </p>
        <div className="testimonials-stars-container">
          <AiFillStar />
          <AiFillStar />
          <AiFillStar />
          <AiFillStar />
          <AiFillStar />
        </div>
        <h2>John Mark Palmer</h2>
      </div>
    </div>
  );
};

export default Testimonial;
