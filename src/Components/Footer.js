import React, { useState } from "react";
import Logo from "../Assets/COMPASS.png";
import './Footer.css';

const Footer = () => {
    const [showTerms, setShowTerms] = useState(false);
    const [showPrivacy, setShowPrivacy] = useState(false);

    const handleTermsClick = () => {
        setShowTerms(true);
    };

    const handlePrivacyClick = () => {
        setShowPrivacy(true);
    };

    return (
        <div className="footer-wrapper">
            <div className="footer-section-one">
                <div className="footer-logo-container">
                    <img src={Logo} alt="" />
                </div>
            </div>
            <div className="footer-section-two">
                <div className="footer-section-columns">
                    <span>1234 Maple Street North East
                        Winnipeg, MB A1B 2C3 Canada</span>
                    <span>2023 All rights reserved</span>
                    <span>📞 +1-885-332-3334</span>
  <span>📨 PLD@lifestyle.ca</span>

                </div>
                <div className="footer-section-columns">
                    <span onClick={handleTermsClick}>Terms & Conditions</span>
                    <span onClick={handlePrivacyClick}>Privacy Policy</span>
                </div>
            </div>
            {showTerms && (
                <div className="popup-wrapper">
                    <div className="popup">
                        <h3>Terms & Conditions</h3>

                        Welcome to PDL Private Masseurs! By accessing or using our website, you agree to be bound by the following terms and conditions:

                        1. User Content: You are responsible for any content that you post on our website. You must not post any content that is illegal, defamatory, or infringes on any third-party rights.

                        2. Intellectual Property: All content on our website is the property of PDL Private Masseurs and is protected by copyright and other intellectual property laws. You may not use any content from our website without our express written permission.

                        3. Disclaimer of Warranties: Our website is provided "as is" without any warranties, express or implied. We do not guarantee that our website will be error-free, secure, or continuously available.

                        4. Limitation of Liability: We will not be liable for any damages arising from your use of our website, including but not limited to direct, indirect, incidental, punitive, and consequential damages.

                        5. Governing Law: These terms and conditions are governed by the laws of the state of Canada and any disputes will be resolved in the courts of that state.
                        <button onClick={() => setShowTerms(false)}>Close</button>
                    </div>
                </div>
            )}
            {showPrivacy && (
                <div className="popup-wrapper">
                    <div className="popup">
                        <h3>Privacy Policy</h3>
                        At PDL Private Masseurs, we take your privacy seriously. This privacy policy outlines how we collect, use, and protect your personal information:

                        1. Information we collect: We collect personal information such as your name, email address, and phone number when you register on our website or request services.

                        2. Use of information: We use your personal information to provide you with our services, communicate with you, and improve our website.

                        3. Sharing of information: We may share your personal information with trusted third-party service providers to help us provide our services. We will not sell, rent, or share your personal information with third-party marketers.

                        4. Security of information: We take reasonable steps to protect your personal information from unauthorized access or disclosure.

                        5. Retention of information: We will retain your personal information for as long as necessary to provide our services and comply with legal obligations.

                        6. Changes to this policy: We may update this privacy policy from time to time. We will notify you of any significant changes to this policy by email or by posting a notice on our website.

                        If you have any questions or concerns about our terms and conditions or privacy policy, please contact us at staff@pdl.com.
                        <button onClick={() => setShowPrivacy(false)}>Close</button>
                    </div>
                </div>
            )}
        </div>
    );
};

export default Footer;