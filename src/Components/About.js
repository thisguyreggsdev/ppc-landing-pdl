import React from "react";
import AboutBackground from "../Assets/about-background.png";
import AboutBackgroundImage from "../Assets/about-background-image.png";


const About = () => {
   const handleClickScroll = (sectionId) => () => {
    const section = document.getElementById(sectionId);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
    <div className="about-section-container">
      <div className="about-background-image-container">
        <img src={AboutBackground} alt="" />
      </div>
      <div className="about-section-image-container">
        <img src={AboutBackgroundImage} alt="" />
      </div>
      <div className="about-section-text-container">
       
        <h1 className="primary-heading">
          We believe that therapeutic massage is an essential part of self-care.
        </h1>
        <p className="primary-text">
          Our expert massage therapists offer a range of techniques, including Swedish, deep tissue, hot stone, and aromatherapy, tailored to your individual needs.
        </p>
        <p className="primary-text">
          Our serene and tranquil atmosphere provides the perfect setting for you to unwind and experience the many benefits of massage
        </p>
        <div className="about-buttons-container">
          <button className="secondary-button" onClick={handleClickScroll('contact')}>Learn More</button>
        </div>
      </div>
    </div>
  );
};

export default About;
