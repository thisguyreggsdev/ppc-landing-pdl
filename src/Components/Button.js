import { CheckCircleIcon, XMarkIcon } from '@heroicons/react/20/solid'
import "./Button.css";

export default function Example() {
  const handleClose = () => {
    window.location.reload();
  };

  return (
    <div className="flex flex-col items-center justify-center h-40 ">
      <div className="success-message">
        <div className="flex">
          <div className="ml-8">
            <p className="text-lg font-medium text-green-800">Successfully sent! Our staff will be in touch with you soon</p>
          </div>
          <div className="ml-auto pl-3">
            <div className="-mx-1.5 -my-1.5">
              <button
                type="button"
                onClick={handleClose}
                className="inline-flex rounded-md bg-green-50 p-1.5 text-green-500 hover:bg-green-100 focus:outline-none focus:ring-2 focus:ring-green-600 focus:ring-offset-2 focus:ring-offset-green-50"
              >
                <span className="sr-only">Dismiss</span>
                <XMarkIcon className="h-5 w-5" aria-hidden="true" />
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="h-20"></div>
    </div>
  );
}
