import React from "react";
import PickMeals from "../Assets/pick-meals-image.png";
import ChooseMeals from "../Assets/choose-image.png";
import DeliveryMeals from "../Assets/delivery-image.png";

const Work = () => {
  const workInfoData = [
    {
      image: PickMeals,
      title: "Choose your massage",
      text: "We offer a range of massage techniques, from relaxing Swedish to deep tissue, hot stone, and aromatherapy. Choose the one that best suits your needs and preferences.",
    },
    {
      image: ChooseMeals,
      title: "Schedule your appointment",
      text: "Our online booking system makes it easy to schedule your massage at a time and date that works for you. You can also call our friendly staff to book your appointment over the phone.",
    },
    {
      image: DeliveryMeals,
      title: "Consult with your therapist",
      text: "Your therapist will consult with you to understand your specific needs and preferences. They'll tailor your massage to your individual requirements, ensuring that you get the most benefit from your session.",
    },
  ];
  return (
    <div className="work-section-wrapper">
      <div className="work-section-top">
        <h1 className="primary-heading">How It Works</h1>
        <p className="primary-text">
          We believe that everyone deserves to experience the many benefits of therapeutic massage. That's why we've made it easy and convenient for you to book and enjoy our services.
        </p>
      </div>
      <div className="work-section-bottom">
        {workInfoData.map((data) => (
          <div className="work-section-info" key={data.title}>
            <div className="info-boxes-img-container">
              <img src={data.image} alt="" />
            </div>
            <h2>{data.title}</h2>
            <p>{data.text}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Work;
