import React, { useState } from "react";
import "./Navbar.css";
import Logo from "../Assets/COMPASS.png";
import { HiOutlineBars3 } from "react-icons/hi2";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import HomeIcon from "@mui/icons-material/Home";
import InfoIcon from "@mui/icons-material/Info";
import CommentRoundedIcon from "@mui/icons-material/CommentRounded";
import PhoneRoundedIcon from "@mui/icons-material/PhoneRounded";

const Navbar = () => {
  const [openMenu, setOpenMenu] = useState(false);
  const menuOptions = [
    {
      text: "Home",
      icon: <HomeIcon />,
      id: ""
    },
    {
      text: "About",
      icon: <InfoIcon />,
      id: "about"
    },
    {
      text: "Testimonials",
      icon: <CommentRoundedIcon />,
      id: "testimonial"
    },
    {
      text: "Contact",
      icon: <PhoneRoundedIcon />,
      id: "contact"
    },
  ];

 const handleClickScroll = (sectionId) => () => {
  console.log('Clicked on:', sectionId);
  const section = document.getElementById(sectionId);
  console.log('Section:', section);
  if (section) {
    section.scrollIntoView({ behavior: 'smooth' });
  }
  setOpenMenu(false); // Close the menu
};

  return (
    <nav>
      <div className="nav-logo-container">
        <img className="logo-img" src={Logo} alt="" />
      </div>
      <ul className="navbar-links-container">
        <li onClick={handleClickScroll('')}>Home</li>
        <li onClick={handleClickScroll('about')}>About</li>
        <li onClick={handleClickScroll('testimonial')}>Testimonials</li>
        <li onClick={handleClickScroll('contact')}>Contact</li>
        <button className="primary-button" onClick={handleClickScroll('contact')}>Book Now</button>
      </ul>
      <div className="navbar-menu-container">
        <HiOutlineBars3 onClick={() => setOpenMenu(true)} />
      </div>
      <Drawer
        open={openMenu}
        onClose={() => setOpenMenu(false)}
        anchor="right"
      >
        <Box
          sx={{ width: 250 }}
          role="presentation"
          onClick={() => setOpenMenu(false)}
          onKeyDown={() => setOpenMenu(false)}
        >
          <List>
          {menuOptions.map((item) => (
          <ListItemButton
            key={item.text}
            onClick={handleClickScroll(
            item.id || item.text.toLowerCase()
            )}
            >
          <ListItemIcon>{item.icon}</ListItemIcon>
          <ListItemText primary={item.text} />
          </ListItemButton>
            ))}
          </List>
          <Divider />
        </Box>
      </Drawer>
    </nav>
  );
};

export default Navbar;
