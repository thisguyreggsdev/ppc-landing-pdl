import React from "react";
import BannerBackground from "../Assets/home-banner-background.png";
import BannerImage from "../Assets/home-banner-image.png";
import Navbar from "./Navbar";
import { FiArrowRight } from "react-icons/fi";

const Home = () => {
   const handleClickScroll = (sectionId) => () => {
    const section = document.getElementById(sectionId);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };
  return (
    <div className="home-container">
      <Navbar />
      <div className="home-banner-container">
        <div className="home-bannerImage-container">
          <img src={BannerBackground} alt="" />
        </div>
        <div className="home-text-section">
          <h1 className="primary-heading">
             Relax and rejuvenate with our private massage sessions
          </h1>
          <p className="primary-text">
            Welcome to PDL Spa, your ultimate destination for relaxation and rejuvenation.
            We offer a variety of luxurious spa treatments that will take you on a journey of self-discovery and serenity.
            Our treatments are designed to cater to all types of individuals, from those seeking a deep tissue massage to those looking for a more tranquil experience.
            Immerse yourself in our healing treatments, indulge in our organic skincare products,
            and create unforgettable memories of tranquility and peace.
            With our expert therapists and carefully crafted treatments, you can be sure to
            have a safe and unforgettable spa experience.
          </p>
          <button className="secondary-button" onClick={handleClickScroll('contact')}>
            Book now <FiArrowRight />{" "}
          </button>
        </div>
        <div className="home-image-section">
          <img src={BannerImage} alt="" />
        </div>
      </div>
    </div>
  );
};

export default Home;
